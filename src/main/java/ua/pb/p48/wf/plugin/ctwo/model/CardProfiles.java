package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class CardProfiles  extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID платежа в BackOffice 
	 */
	@XmlElement
	private Long id;
	/**
	 * описанеи платежа
	 */
	@XmlElement
	private String title;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @return the {@linkplain #title}
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the {@linkplain #title} to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @param id the {@linkplain #id} to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
