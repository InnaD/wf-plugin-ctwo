package ua.pb.p48.wf.plugin.ctwo.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.*;

import javax.annotation.Resource;

import oracle.jdbc.OracleTypes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jdbc.support.oracle.SqlArrayValue;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnType;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import ua.pb.p48.wf.plugin.base.dao.impl.AbstractBaseDao;
import ua.pb.p48.wf.plugin.base.ds.ShardedDataSource;
import ua.pb.p48.wf.plugin.ctwo.dao.CtwoDao;
import ua.pb.p48.wf.plugin.ctwo.model.*;


@Repository
public class CtwoDaoImpl extends AbstractBaseDao implements CtwoDao {

	private static final transient Logger LOG = LoggerFactory.getLogger(CtwoDaoImpl.class);

	@Value("#{sqlCtwoProperties['get_card_status']}")
	private String get_card_status;
	@Value("#{sqlCtwoProperties['get_card_info']}")
	private String get_card_info;
	@Value("#{sqlCtwoProperties['get_acct_info']}")
	private String get_acct_info;
	@Value("#{sqlCtwoProperties['get_card_limit']}")
	private String get_card_limit;
	@Value("#{sqlCtwoProperties['get_account_limit']}")
	private String get_account_limit;
	@Value("#{sqlCtwoProperties['cardbatch']}")
	private String cardbatch;
	@Value("#{sqlCtwoProperties['getCardInfoLight']}")
	private String getCardInfoLight;
	@Value("#{sqlCtwoProperties['accountbatch']}")
	private String accountbatch;
	@Value("#{sqlCtwoProperties['getAccountsByCardLight']}")
	private String getAccountsByCardLight;
	@Value("#{sqlCtwoProperties['getAccountInfoLight']}")
	private String getAccountInfoLight;
	@Value("#{sqlCtwoProperties['copy_crd_limits']}")
	private String copy_crd_limits;
	@Value("#{sqlCtwoProperties['get_card_account_limits']}")
	private String get_card_account_limits;
	@Value("#{sqlCtwoProperties['copy_acc_limits']}")
	private String copy_acc_limits;


	@Resource(name = "dsUri2SchemaMapCtwo")
	private Map<String, String> dsUri2SchemaMap;

	@Resource(name = "dataUri2FiidMapCtwo")
	private Map<String, String> dataUri2FiidMap;

	@Autowired
	public CtwoDaoImpl(@Qualifier("dbctwo") ShardedDataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	public String getCrdStat(String pan) {
		SimpleJdbcCall getCrdStat = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(get_card_status)).declareParameters(
				new SqlParameter("pPan",Types.VARCHAR),
				new SqlOutParameter("pState",Types.VARCHAR));

		Map<String, Object> result = getCrdStat.execute(pan);


		String crdStat = (String)result.get("pState");

		LOG.debug("getCrdStat pan:{} crdStat:{}",pan,crdStat);
		if (crdStat.equals("-1")) {
			crdStat = null;
		}
		return crdStat;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public CardInfo getCardInfo(String pan) {
		SimpleJdbcCall getCardInfo = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(get_card_info))
		.declareParameters(
				new SqlOutParameter("result",Types.INTEGER),
				new SqlParameter("pPAN", Types.VARCHAR),
				new SqlOutParameter("pCardUID", Types.VARCHAR),
				new SqlOutParameter("pStatus", Types.INTEGER),
				new SqlOutParameter("pExpDate", Types.DATE),
				new SqlOutParameter("pLastTranId", Types.BIGINT),
				new SqlOutParameter("pLastATMUsed", Types.DATE),
				new SqlOutParameter("pLastPOSUsed", Types.DATE),
				new SqlOutParameter("pLastRefreshTime", Types.DATE),
				new SqlOutParameter("pLastChangeStatusTime", Types.DATE),
				new SqlOutParameter("pLastTranTime", Types.DATE),
				new SqlOutParameter("pAccounts", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new AccountsRowMapper())),
								new SqlOutParameter("pCardProfiles",
										OracleTypes.CURSOR, null,
										new ReturnOptionalRefCursor(
												new CardProfilesRowMapper()))).withReturnValue();

		Map<String, Object> result = getCardInfo.execute(pan);

		//Проверка найдена ли карта (согл. описания, что дал процессинг)
		int res = (Integer) result.get("result");
		if (res!=1) {
			return null;
		}

		CardInfo cardInfo = new CardInfo();

		cardInfo.setCardUID((String) result.get("pCardUID"));
		cardInfo.setStatus((Integer) result.get("pStatus"));
		cardInfo.setExpDate((Date) result.get("pExpDate"));
		cardInfo.setLastTranId((Long) result.get("pLastTranId"));
		cardInfo.setLastATMUsed((Date) result.get("pLastATMUsed"));
		cardInfo.setLastPOSUsed((Date) result.get("pLastPOSUsed"));
		cardInfo.setLastRefreshTime((Date) result.get("pLastRefreshTime"));
		cardInfo.setLastChangeStatusTime((Date) result
				.get("pLastChangeStatusTime"));
		cardInfo.setLastTranTime((Date) result.get("pLastTranTime"));
		cardInfo.setAccounts((List) result.get("pAccounts"));
		cardInfo.setCardProfiles((List) result.get("pCardProfiles"));

		return cardInfo;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public AcctInfo getAcctInfo(String pAccount, String dataUri) {
		SimpleJdbcCall getAcctInfo = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withFunctionName(fmsWithSchema(get_acct_info)).declareParameters(
				new SqlOutParameter("result",Types.INTEGER),
				new SqlParameter("pAccount",Types.VARCHAR),
				new SqlParameter("pFIID",Types.INTEGER),
				new SqlOutParameter("pStatus",Types.INTEGER),
				new SqlOutParameter("pCurrency",Types.INTEGER),
				new SqlOutParameter("pLedger",Types.NUMERIC),
				new SqlOutParameter("pAvail",Types.NUMERIC),
				new SqlOutParameter("pDebitHold",Types.NUMERIC),
				new SqlOutParameter("pCreditHold",Types.NUMERIC),
				new SqlOutParameter("pBonus",Types.NUMERIC),
				new SqlOutParameter("pOverdraft",Types.NUMERIC),
				new SqlOutParameter("pTmpOverdraft",Types.NUMERIC),
				new SqlOutParameter("pTmpOverdraftExpiration",Types.DATE),
				new SqlOutParameter("pRemain",Types.NUMERIC),
				new SqlOutParameter("pCards",OracleTypes.CURSOR,null,new ReturnOptionalRefCursor(new CardRowMapper())))
				.withReturnValue();

		Map<String, Object> result = getAcctInfo.execute(pAccount,
				getFiidByDataUri(dataUri));

		//Проверка найдена ли карта (согл. описания, что дал процессинг)
		int res = (Integer) result.get("result");
		if (res!=1) {
			return null;
		}

		AcctInfo accInfo = new AcctInfo();
		accInfo.setpAvail((BigDecimal)result.get("pAvail"));
		accInfo.setpBonus((BigDecimal)result.get("pBonus"));
		accInfo.setpCreditHold((BigDecimal)result.get("pCreditHold"));
		accInfo.setpCurrency((Integer)result.get("pCurrency"));
		accInfo.setpDebitHold((BigDecimal)result.get("pDebitHold"));
		accInfo.setpLedger((BigDecimal)result.get("pLedger"));
		accInfo.setpOverdraft((BigDecimal)result.get("pOverdraft"));
		accInfo.setpTmpOverdraft((BigDecimal)result.get("pTmpOverdraft"));
		accInfo.setpTmpOverdraftExpiration((Date)result.get("pTmpOverdraftExpiration"));
		accInfo.setpRemain((BigDecimal)result.get("pRemain"));
		accInfo.setpStatus((Integer)result.get("pStatus"));
		accInfo.setpTmpOverdraft((BigDecimal)result.get("pTmpOverdraft"));
		accInfo.setpTmpOverdraftExpiration((Date)result.get("pTmpOverdraftExpiration"));
		accInfo.setpCards((List)result.get("pCards"));

		return accInfo;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<LimitInfo> getCardLimit(String pan) {
		SimpleJdbcCall getCardLimit = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(get_card_limit))
		.declareParameters(
				new SqlParameter("pPAN", Types.VARCHAR),
				new SqlOutParameter("pLimits", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new CardLimitRowMapper())));

		Map<String, Object> result = getCardLimit.execute(pan);

		List<LimitInfo> limitInfo = ((List) result.get("pLimits"));
		if (limitInfo == null || limitInfo.isEmpty()) {
			return null;
		}
		return limitInfo;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<LimitInfo> getAccountLimit(String pAccount, String dataUri) {
		SimpleJdbcCall getAccountLimit = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(get_account_limit))
		.declareParameters(
				new SqlParameter("pFIID", Types.INTEGER),
				new SqlParameter("pPAN", Types.VARCHAR),
				new SqlOutParameter("pLimits", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new AccountLimitRowMapper())));

		Map<String, Object> result = getAccountLimit.execute(
				getFiidByDataUri(dataUri), pAccount);

		List<LimitInfo> limitInfo = ((List) result.get("pLimits"));
		if (limitInfo == null || limitInfo.isEmpty()) {
			return null;
		}

		return limitInfo;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<CardInfo> getCardInfoLight(List<String> cards) {

		Object[] batchObj = new Object[cards.size()];
		int i = 0;
		for (String card : cards) {
			batchObj[i] = card;
			i++;
		}

		SimpleJdbcCall cardInfoLight = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(getCardInfoLight))
		.declareParameters(
				new SqlParameter("pCards", OracleTypes.ARRAY, fmsWithSchema(cardbatch).toUpperCase()),
				new SqlOutParameter("pCardInfo", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new CardInfoRowMapper())));
		Map<String,Object> in  = new HashMap<String,Object>();
		in.put("pCards", new SqlArrayValue(batchObj));

		Map<String,Object> result = cardInfoLight.execute( in );

		List<CardInfo> resultList = (List)result.get("pCardInfo");

		return resultList;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<AccountInfoLight> getAccountsByCardLight(List<String> cards) {
		Object[] batchObj = new Object[cards.size()];
		int i = 0;
		for (String card : cards) {
			batchObj[i] = card;
			i++;
		}

		SimpleJdbcCall cardInfoLight = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(getAccountsByCardLight))
		.declareParameters(
				new SqlParameter("pCards", OracleTypes.ARRAY, fmsWithSchema(cardbatch).toUpperCase()),
				new SqlOutParameter("pAccounts", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new AccountInfoRowMapper())));
		Map<String, Object> in = new HashMap<String, Object>();
		in.put("pCards", new SqlArrayValue(batchObj));

		Map<String, Object> result = cardInfoLight.execute(in);

		List<AccountInfoLight> resultList = (List) result.get("pAccounts");

		return resultList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<AccountInfoLight> getAccountInfoLight(List<AccountInfoLightRequest> accInfRequest) {
		Object[][] batchObj = new Object[accInfRequest.size()][2];
		int i = 0;
		for (AccountInfoLightRequest accReq : accInfRequest) {
			initRequestArrayRow(batchObj, accReq, i);
			i++;
		}
		SimpleJdbcCall cardInfoLight = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(getAccountInfoLight))
		.declareParameters(
				new SqlParameter("pAccounts", OracleTypes.ARRAY, fmsWithSchema(accountbatch).toUpperCase()),
				new SqlOutParameter("pAccountInfo", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new AccountInfoLightRowMapper())));

		Map<String, Object> in = new HashMap<String, Object>();
		in.put("pAccounts", new SqlArrayValue(batchObj,"AccountID"));

		Map<String, Object> result = cardInfoLight.execute(in);

		List<AccountInfoLight> resultList = (List) result.get("pAccountInfo");

		return resultList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<LimitInfo> getCardAccountLimit(String pan) {
		SimpleJdbcCall getCardLimit = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withProcedureName(fmsWithSchema(get_card_account_limits))
		.declareParameters(
				new SqlParameter("pPAN", Types.VARCHAR),
				new SqlOutParameter("pLimits", OracleTypes.CURSOR,
						null, new ReturnOptionalRefCursor(
								new CardLimitRowMapper())));

		Map<String, Object> result = getCardLimit.execute(pan);

		List<LimitInfo> limitInfo = ((List) result.get("pLimits"));
		if (limitInfo == null || limitInfo.isEmpty()) {
			return null;
		}
		return limitInfo;
	}

	private void initRequestArrayRow(Object[][] recObj, AccountInfoLightRequest accReq, int j) {
		recObj[j][0] = 	getFiidByDataUri(accReq.getDataUri());
		recObj[j][1] = 	accReq.getCan();

	}

	/**
	 * Класс для отлова exception в случ. если коллеги вернут пустой реф. курсор
	 * @author kpi
	 */
	@SuppressWarnings("rawtypes")
	private class ReturnOptionalRefCursor implements SqlReturnType {

		private RowMapper rowMapper;

		ReturnOptionalRefCursor(RowMapper rowMapper) {
			this.rowMapper =rowMapper;
		}

		@Override
		@SuppressWarnings("unchecked")
		public Object getTypeValue(CallableStatement cs, int ix, int sqlType, String typeName) throws SQLException {
			try {
				ResultSet rs = (ResultSet) cs.getObject(ix);
				List l = new ArrayList();
				if(rs != null){
					for (int i = 0; rs.next(); i++) {
						l.add(rowMapper.mapRow(rs, i));
					}
				}
				return l;
			}
			catch (SQLException e) {
				// check the exception and ignore certain exceptions
				if (e.getMessage() != null && e.getMessage().startsWith("Cursor is closed")) {
					return new ArrayList();
				}
				else {
					throw e;
				}
			}
		}
	}

	/**
	 * Маппер для массива карт в AccInfo
	 * @author kpi
	 *
	 */
	@SuppressWarnings("rawtypes")
	private static class CardRowMapper implements RowMapper {
		@Override
		public Object mapRow(ResultSet pRs, int pRowNum) throws SQLException {
			Card card = new Card();
			card.setCarddescr(pRs.getString("carddescr"));
			card.setCardUID(pRs.getString("cardUID"));
			card.setMbr(pRs.getInt("mbr"));
			card.setPan(pRs.getString("pan"));
			card.setStatus(AbstractBaseDao.convertToHexadecimal(pRs.getString("status")));

			return card;
		}
	}


	@SuppressWarnings("rawtypes")
	private static class AccountsRowMapper implements RowMapper {
		@Override
		public Object mapRow(ResultSet pRs, int pRowNum) throws SQLException {
			Accounts account = new Accounts();
			account.setAccount(pRs.getString("account"));
			account.setStatus(AbstractBaseDao.convertToHexadecimal(pRs.getString("status")));
			account.setAcctdescr(pRs.getString("acctdescr"));
			account.setLedgerbalance(pRs.getBigDecimal("ledgerbalance"));
			account.setAvailbalance(pRs.getBigDecimal("availbalance"));
			account.setCurrency(pRs.getLong("currency"));
			return account;
		}
	}

	@SuppressWarnings("rawtypes")
	private static class CardProfilesRowMapper implements RowMapper {
		@Override
		public Object mapRow(ResultSet pRs, int pRowNum) throws SQLException {
			CardProfiles cardProfiles = new CardProfiles();
			cardProfiles.setId(pRs.getLong("id"));
			cardProfiles.setTitle(pRs.getString("title"));
			return cardProfiles;
		}
	}


	@SuppressWarnings("rawtypes")
	private static class CardLimitRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			LimitInfo limitInfo = new LimitInfo();
			limitInfo.setLimitId(rs.getLong("limitid"));
			limitInfo.setMaxValue(rs.getBigDecimal("maxvalue"));
			limitInfo.setResetTime(rs.getString("resettime"));
			return limitInfo;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class AccountLimitRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			LimitInfo limitInfo = new LimitInfo();
			limitInfo.setLimitId(rs.getLong("limitid"));
			limitInfo.setMaxValue(rs.getBigDecimal("maxvalue"));
			limitInfo.setResetTime(rs.getString("resettime"));
			return limitInfo;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class CardInfoRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			CardInfo cardInfo = new CardInfo();
			cardInfo.setPan(rs.getString("pan"));
			cardInfo.setStatus(rs.getInt("status"));
			cardInfo.setExpDate(rs.getDate("expDate"));
			cardInfo.setLastTranId(rs.getLong("lastTranId"));
			cardInfo.setLastATMUsed(rs.getDate("lastATMUsed"));
			cardInfo.setLastPOSUsed(rs.getDate("lastPOSUsed"));
			cardInfo.setLastRefreshTime(rs.getDate("lastRefreshTime"));
			cardInfo.setLastChangeStatusTime(rs.getDate("lastChangeStatusTime"));
			cardInfo.setLastTranTime(rs.getDate("lastTranTime"));

			return cardInfo;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class AccountInfoRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			AccountInfoLight accInfo = new AccountInfoLight();
			accInfo.setPan(rs.getString("pan"));
			accInfo.setStatus(rs.getLong("status"));
			accInfo.setAccount(rs.getString("account"));
			accInfo.setAcctdescr(rs.getString("acctdescr"));
			accInfo.setLedgerbalance(rs.getBigDecimal("ledgerbalance"));
			accInfo.setAvailbalance(rs.getBigDecimal("availbalance"));

			return accInfo;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class AccountInfoLightRowMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			AccountInfoLight accInfo = new AccountInfoLight();
			accInfo.setFiid(rs.getInt("fiid"));
			accInfo.setAccount(rs.getString("account"));
			accInfo.setStatus(rs.getLong("status"));
			accInfo.setCurrency(rs.getLong("currency"));
			accInfo.setLedgerbalance(rs.getBigDecimal("ledgerbalance"));
			accInfo.setAvailbalance(rs.getBigDecimal("availbalance"));
			accInfo.setDebithold(rs.getBigDecimal("debithold"));
			accInfo.setCredithold(rs.getBigDecimal("credithold"));
			accInfo.setBonus(rs.getBigDecimal("bonus"));
			accInfo.setOverdraftlimit(rs.getBigDecimal("overdraftlimit"));
			accInfo.setTmpoverdraft(rs.getBigDecimal("tmpoverdraft"));
			accInfo.setTmpoverdraftexpiration(rs.getDate("tmpoverdraftexpiration"));
			accInfo.setRemain(rs.getBigDecimal("remain"));


			return accInfo;
		}

	}

	/**
	 * Для форматирования sql с учетом банка
	 * @param sql
	 * @return
	 */
	public String fmsWithSchema(String sql) {
		return MessageFormat.format(sql,  getSchemaByDsUri(getCurrentDsUri()));
	}

	public String getSchemaByDsUri(String dsUri) {
		String res = dsUri2SchemaMap.get(dsUri);
		if (res==null) {
			throw new CtwoException(CtwoException.CODE_NOSFBA, "No schema for dsUri:"+dsUri);
		}
		return res;
	}

	public Integer getFiidByDataUri(String dataUri){
		if(dataUri == null){
			throw new IllegalArgumentException("Input parameter dataUri can't be null");
		}
		Integer res = Integer.valueOf(dataUri2FiidMap.get(dataUri.toLowerCase()));
		if (res==null) {
			throw new CtwoException(CtwoException.CODE_NOSFBA, "No fiid for dataUri:"+dataUri);
		}
		return res;
	}

	@Override
	public Integer copyCrdLimits(String pFromPAN, String pToPAN) {
		SimpleJdbcCall сopyСrdLimits = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withFunctionName(fmsWithSchema(copy_crd_limits))
		.declareParameters(
				new SqlOutParameter("res",Types.INTEGER),
				new SqlParameter("pFromPAN", Types.VARCHAR),
				new SqlParameter("pToPAN", Types.VARCHAR)).withReturnValue();
		Map<String, Object> result = сopyСrdLimits.execute(pFromPAN, pToPAN);

		Integer res = (Integer) result.get("res");
		return res;
	}

	@Override
	public Integer copyAccLimits(String dataUri, String pFromAcc, String dataUriPanOld, String pToAcc) {
		SimpleJdbcCall сopyAccLimits = new SimpleJdbcCall(getJdbcTemplate())
		.withoutProcedureColumnMetaDataAccess()
		.withFunctionName(fmsWithSchema(copy_acc_limits))
		.declareParameters(new SqlOutParameter("res", Types.INTEGER),
				new SqlParameter("pFromFIID", Types.NUMERIC),
				new SqlParameter("pFromACC", Types.VARCHAR),
				new SqlParameter("pToFIID", Types.NUMERIC),
				new SqlParameter("pToACC", Types.VARCHAR))
				.withReturnValue();
		Map<String, Object> result = сopyAccLimits.execute(
				getFiidByDataUri(dataUri), pFromAcc,
				getFiidByDataUri(dataUriPanOld), pToAcc);

		Integer res = (Integer) result.get("res");
		return res;
	}

}