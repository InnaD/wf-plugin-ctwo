package ua.pb.p48.wf.plugin.ctwo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.pb.p48.wf.plugin.base.dao.BaseDao;
import ua.pb.p48.wf.plugin.base.model.PluginResult;
import ua.pb.p48.wf.plugin.base.service.LogPluginService;
import ua.pb.p48.wf.plugin.base.service.RequestConverter;
import ua.pb.p48.wf.plugin.base.service.impl.AbstractBasePluginService;
import ua.pb.p48.wf.plugin.base.util.PluginUtil;
import ua.pb.p48.wf.plugin.ctwo.dao.CtwoDao;
import ua.pb.p48.wf.plugin.ctwo.model.*;
import ua.pb.p48.wf.plugin.ctwo.service.CtwoService;

import com.google.common.collect.Lists;


@Service
@WebService(endpointInterface = "ua.pb.p48.wf.plugin.ctwo.service.CtwoService")
public class CtwoServiceImpl extends AbstractBasePluginService implements CtwoService {

	private static final transient Logger LOG = LoggerFactory.getLogger(CtwoServiceImpl.class);
	private static final int MAX_SIZE = 50;

	@Resource(name = "bank2dsMapCtwo")
	private Map<String, String> bank2dsMap;
	
	@Autowired
	@Qualifier(value="pluginCtwo")
	private LogPluginService logPluginService;

	@Override
	public String getUriByBank(String bank) {
		return bank2dsMap.get(bank.toUpperCase());
	}

	@Autowired
	private CtwoDao ctwoDao;


	public CtwoDao getCtwoDao(String dsUri) {
		getCtwoDao().setDsUri(dsUri);
		return getCtwoDao();
	}

	@Override
	public Set<String> getDsUriList() {
		return getCtwoDao().getDsUriSet();
	}



	public CtwoDao getCtwoDao() {
		return ctwoDao;
	}

	@Override
	@WebMethod(exclude=true)
	public BaseDao getDao() {
		return getCtwoDao();
	}

	@Override
	public void setDsUri(String dsUri) {
		getCtwoDao().setDsUri(dsUri);
	}


	@Override
	@Deprecated
	@Transactional(value = "dbCtwoTx")
	public String getCrdStat(String pan) {
		LOG.debug("getCrdStat pan:{}",pan);
		return getCtwoDao().getCrdStat(pan);
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public AcctInfo getAcctInfoWithDataUri(final String pAccount, final String dataUri) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pAccount, dataUri), methodName,reference);
		final AcctInfo acctInfo = getCtwoDao().getAcctInfo(pAccount, dataUri);
		logPluginService.responseLog(RequestConverter.responsValue(acctInfo), methodName,reference);
		return acctInfo;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public CardInfo getCardInfo(final String pan) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pan), methodName,reference);
		final CardInfo cardInfo = getCtwoDao().getCardInfo(pan);
		logPluginService.responseLog(RequestConverter.responsValue(cardInfo), methodName,reference);
		return cardInfo;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<LimitInfo> getCardLimit(final String pan) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pan), methodName,reference);
		final List<LimitInfo> limitInfoList = getCtwoDao().getCardLimit(pan);
		logPluginService.responseLog(RequestConverter.responsValue(limitInfoList), methodName,reference);
		return limitInfoList;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<LimitInfo> getAccountLimit(final String pAccount, final String dataUri) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pAccount, dataUri), methodName,reference);
		final List<LimitInfo> limitInfoList = getCtwoDao().getAccountLimit(pAccount, dataUri);
		logPluginService.responseLog(RequestConverter.responsValue(limitInfoList), methodName,reference);
		return limitInfoList;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<CardInfo> getCardInfoLight(final List<String> cards) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(cards), methodName,reference);
		final List<CardInfo> cardInfoList = new ArrayList<CardInfo>();
		final List<List<String>> subLists = Lists.partition(cards, MAX_SIZE);
		for (final List<String> list : subLists) {
			final List<CardInfo> part = getCtwoDao().getCardInfoLight(list);
			if (part !=null && !part.isEmpty()) {
				cardInfoList.addAll(part);
			}
		}
		logPluginService.responseLog(RequestConverter.responsValue(cardInfoList), methodName,reference);
		return cardInfoList;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<AccountInfoLight> getAccountsByCardLight(List<String> cards) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(cards), methodName,reference);
		final List<AccountInfoLight> accountsList = getCtwoDao().getAccountsByCardLight(cards);
		logPluginService.responseLog(RequestConverter.responsValue(accountsList), methodName,reference);
		return accountsList;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<AccountInfoLight> getAccountInfoLight(final List<AccountInfoLightRequest> accInfoRequest) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(accInfoRequest), methodName,reference);
		final List<AccountInfoLight> accInfo = new ArrayList<AccountInfoLight>();
		final List<List<AccountInfoLightRequest>> subLists = Lists.partition(accInfoRequest, MAX_SIZE);
		for (final List<AccountInfoLightRequest> list : subLists) {
			final List<AccountInfoLight> part = getCtwoDao().getAccountInfoLight(list);
			if (part != null && !part.isEmpty()) {
				accInfo.addAll(part);
			}
		}
		logPluginService.responseLog(RequestConverter.responsValue(accInfo), methodName,reference);
		return accInfo;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public CopyCrdLimitResult copyCrdLimits(final String pFromPAN, final String pToPAN) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pFromPAN, pToPAN), methodName,reference);
		final CopyCrdLimitResult result = new CopyCrdLimitResult();
		result.setResult(getCtwoDao().copyCrdLimits(pFromPAN, pToPAN));
		if (result.getResult() == null || result.getResult() == -1) {
			result.setPrState(PluginResult.PRSTATE_ERROR);
			result.setPrCode(PluginResult.PRCODE_ERROR);
		} else {
			result.setPrState(PluginResult.PRSTATE_OK);
			result.setPrCode(PluginResult.PRCODE_OK);
		}
		logPluginService.responseLog(RequestConverter.responsValue(result), methodName,reference);
		return result;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public List<LimitInfo> getCardAccountLimit(final String pan) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(pan), methodName,reference);
		final List<LimitInfo> limitInfoList = getCtwoDao().getCardAccountLimit(pan);
		logPluginService.responseLog(RequestConverter.responsValue(limitInfoList), methodName,reference);
		return limitInfoList;
	}

	@Override
	@Transactional(value = "dbCtwoTx")
	public CopyCrdLimitResult copyAccLimits(final String dataUri, final String pFromAcc, final String dataUriPanOld, final String pToAcc) {
		String reference = PluginUtil.generateString(25);
		final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		logPluginService.requestLog(RequestConverter.requestValue(dataUri, pFromAcc, dataUriPanOld), methodName,reference);
		final CopyCrdLimitResult result = new CopyCrdLimitResult();
		result.setResult(getCtwoDao().copyAccLimits(dataUri, pFromAcc, dataUriPanOld, pToAcc));
		if (result.getResult() == null || result.getResult() == -1) {
			result.setPrState(PluginResult.PRSTATE_ERROR);
			result.setPrCode(PluginResult.PRCODE_ERROR);
		} else {
			result.setPrState(PluginResult.PRSTATE_OK);
			result.setPrCode(PluginResult.PRCODE_OK);
		}
		logPluginService.responseLog(RequestConverter.responsValue(result), methodName,reference);
		return result;
	}
}