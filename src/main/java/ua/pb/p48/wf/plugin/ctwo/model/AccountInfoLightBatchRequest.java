package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * Класс для запроса информации по счету
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class AccountInfoLightBatchRequest extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * список Классов для запроса информации по счету
	 */
	@XmlElementWrapper(name="accountInfoLightRequestList")
	@XmlElement(name="accountInfoLightRequest")
	private List<AccountInfoLightRequest> accountInfoLightRequestList;

	/**
	 * @return the {@linkplain #accountInfoLightRequestList}
	 */
	public List<AccountInfoLightRequest> getAccountInfoLightRequestList() {
		return accountInfoLightRequestList;
	}

	/**
	 * @param accountInfoLightRequestList the  {@linkplain #accountInfoLightRequestList} to set
	 */
	public void setAccountInfoLightRequestList(
			List<AccountInfoLightRequest> accountInfoLightRequestList) {
		this.accountInfoLightRequestList = accountInfoLightRequestList;
	}
}