package ua.pb.p48.wf.plugin.ctwo.model;

/**
 * Статусы кард CTWO
 * @author Rat
 *
 */
public class CrdStatConst {
	
	public static final String NOT_ACTIVE = "0"; 
	public static final String OPENED = "1"; 
	public static final String LOST = "2"; 
	public static final String STOLEN = "3"; 
	public static final String RESTRICTED = "4"; 
	public static final String VIP = "5"; 
	public static final String INTERNAL = "6"; 
	public static final String COMPROMISED = "8"; 
	public static final String CLOSED = "9"; 
	public static final String REFERRAL = "A"; 
	public static final String DECLARED = "C"; 
	public static final String EXPIRED = "F";
	
	public static final int NOT_ACTIVE_INT = 0; 
	public static final int OPENED_INT = 1; 
	public static final int LOST_INT = 2; 
	public static final int STOLEN_INT = 3; 
	public static final int RESTRICTED_INT = 4; 
	public static final int VIP_INT = 5; 
	public static final int INTERNAL_INT = 6; 
	public static final int COMPROMISED_INT = 8; 
	public static final int CLOSED_INT = 9; 
	public static final int REFERRAL_INT = 10; 
	public static final int DECLARED_INT = 12; 
	public static final int EXPIRED_INT = 15;
	
	private static final String NOT_ACTIVE_NAME_RU = "Не активна"; 
	private static final String OPENED_NAME_RU = "Открыта"; 
	private static final String LOST_NAME_RU = "Потеряна"; 
	private static final String STOLEN_NAME_RU = "Украдена"; 
	private static final String RESTRICTED_NAME_RU = "Ограничена"; 
	private static final String VIP_NAME_RU = "VIP"; 
	private static final String INTERNAL_NAME_RU = "Внутренняя"; 
	private static final String COMPROMISED_NAME_RU = "Скомпрометирована"; 
	private static final String CLOSED_NAME_RU = "Закрыта"; 
	private static final String REFERRAL_NAME_RU = "До выяснения"; 
	private static final String DECLARED_NAME_RU = "Заказана"; 
	private static final String EXPIRED_NAME_RU = "Просрочена";
	
	private static final String NOT_ACTIVE_NAME_EN = "NOT ACTIVE"; 
	private static final String OPENED_NAME_EN = "OPENED"; 
	private static final String LOST_NAME_EN = "LOST"; 
	private static final String STOLEN_NAME_EN = "STOLEN"; 
	private static final String RESTRICTED_NAME_EN = "RESTRICTED"; 
	private static final String VIP_NAME_EN = "VIP"; 
	private static final String INTERNAL_NAME_EN = "INTERNAL"; 
	private static final String COMPROMISED_NAME_EN = "COMPROMISED"; 
	private static final String CLOSED_NAME_EN = "CLOSED"; 
	private static final String REFERRAL_NAME_EN = "REFERRAL"; 
	private static final String DECLARED_NAME_EN = "DECLARED"; 
	private static final String EXPIRED_NAME_EN = "EXPIRED";
	
	public static String getCardStatNameRu(String stat) {
		if (stat.equals(NOT_ACTIVE)) 
			return NOT_ACTIVE_NAME_RU;
		else if (stat.equals(OPENED)) 
			return OPENED_NAME_RU;
		else if (stat.equals(LOST)) 
			return LOST_NAME_RU;
		else if (stat.equals(STOLEN)) 
			return STOLEN_NAME_RU;
		else if (stat.equals(RESTRICTED)) 
			return RESTRICTED_NAME_RU;
		else if (stat.equals(VIP)) 
			return VIP_NAME_RU;
		else if (stat.equals(INTERNAL)) 
			return INTERNAL_NAME_RU;
		else if (stat.equals(COMPROMISED)) 
			return COMPROMISED_NAME_RU;
		else if (stat.equals(CLOSED)) 
			return CLOSED_NAME_RU;
		else if (stat.equals(REFERRAL)) 
			return REFERRAL_NAME_RU;
		else if (stat.equals(DECLARED)) 
			return DECLARED_NAME_RU;
		else if (stat.equals(EXPIRED)) 
			return EXPIRED_NAME_RU;
		else
			return "";
	}
	
	public static String getCardStatNameEn(String stat) {
		if (stat.equals(NOT_ACTIVE)) 
			return NOT_ACTIVE_NAME_EN;
		else if (stat.equals(OPENED)) 
			return OPENED_NAME_EN;
		else if (stat.equals(LOST)) 
			return LOST_NAME_EN;
		else if (stat.equals(STOLEN)) 
			return STOLEN_NAME_EN;
		else if (stat.equals(RESTRICTED)) 
			return RESTRICTED_NAME_EN;
		else if (stat.equals(VIP)) 
			return VIP_NAME_EN;
		else if (stat.equals(INTERNAL)) 
			return INTERNAL_NAME_EN;
		else if (stat.equals(COMPROMISED)) 
			return COMPROMISED_NAME_EN;
		else if (stat.equals(CLOSED)) 
			return CLOSED_NAME_EN;
		else if (stat.equals(REFERRAL)) 
			return REFERRAL_NAME_EN;
		else if (stat.equals(DECLARED)) 
			return DECLARED_NAME_EN;
		else if (stat.equals(EXPIRED)) 
			return EXPIRED_NAME_EN;
		else
			return stat;
	}

}
