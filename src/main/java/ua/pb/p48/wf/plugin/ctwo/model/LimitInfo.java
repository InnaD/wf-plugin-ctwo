package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * Класс для получения лимита 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class LimitInfo extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * ид лимита
	 */
	@XmlElement
	private Long limitId;
	/**
	 * размер лимита
	 */
	@XmlElement	
	private BigDecimal maxValue;
	/**
	 * время установки
	 */
	@XmlElement
	private String resetTime;
	/**
	 * @return the {@linkplain #limitId}
	 */
	public Long getLimitId() {
		return limitId;
	}
	/**
	 * @param limitId the {@linkplain #limitId} to set
	 */
	public void setLimitId(Long limitId) {
		this.limitId = limitId;
	}
	/**
	 * @return the {@linkplain #maxValue}
	 */
	public BigDecimal getMaxValue() {
		return maxValue;
	}
	/**
	 * @param maxValue the {@linkplain #maxValue} to set
	 */
	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}
	/**
	 * @return the {@linkplain #resetTime}
	 */
	public String getResetTime() {
		return resetTime;
	}
	/**
	 * @param resetTime the {@linkplain #resetTime} to set
	 */
	public void setResetTime(String resetTime) {
		this.resetTime = resetTime;
	}
}