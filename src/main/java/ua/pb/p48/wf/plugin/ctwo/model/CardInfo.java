package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;
/**
 * 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class CardInfo extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Уникальный идентификатор карты в рамках сессии
	 */
	@XmlElement
	private String cardUID;
	/**
	 * 
	 */
	@XmlElement
	private String pan;
	/**
	 * Статус карты 
	 */
	@XmlElement
	private Integer status;
	/**
	 * Срок действия карты 
	 */
	@XmlElement
	private Date expDate;
	/**
	 * Ид. последней транзакции по карте 
	 */
	@XmlElement
	private Long lastTranId;
	/**
	 * Время последнего использования карты в банкомате 
	 */
	@XmlElement
	private Date lastATMUsed;
	/**
	 * Время последнего использования карты в POS-терминале 
	 */
	@XmlElement
	private Date lastPOSUsed;
	/**
	 * Время последнего обновления карты процедурой Refresh 
	 */
	@XmlElement
	private Date lastRefreshTime;
	/**
	 * Время последнего изменения статуса карты в TWO 
	 */
	@XmlElement
	private Date lastChangeStatusTime;
	/**
	 * Время последней транзакции 
	 */
	@XmlElement
	private Date lastTranTime;
	
	/**
	 * 
	 */
	@XmlElement (name = "account")
	@XmlElementWrapper(name = "accounts")
	private List<Accounts> accounts;
	
	/**
	 * 
	 */
	@XmlElement (name = "cardProfile")
	@XmlElementWrapper(name = "cardProfiles")
	private List<CardProfiles> cardProfiles;

	/**
	 * @return the {@linkplain #cardUID}
	 */
	public String getCardUID() {
		return cardUID;
	}

	/**
	 * @param cardUID the {@linkplain #cardUID} to set
	 */
	public void setCardUID(String cardUID) {
		this.cardUID = cardUID;
	}

	/**
	 * @return the {@linkplain #status}
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the {@linkplain #status} to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the {@linkplain #expDate}
	 */
	public Date getExpDate() {
		return expDate;
	}

	/**
	 * @param expDate the {@linkplain #expDate} to set
	 */
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	/**
	 * @return the {@linkplain #lastTranId}
	 */
	public Long getLastTranId() {
		return lastTranId;
	}

	/**
	 * @param lastTranId the {@linkplain #lastTranId} to set
	 */
	public void setLastTranId(Long lastTranId) {
		this.lastTranId = lastTranId;
	}

	/**
	 * @return the {@linkplain #lastATMUsed}
	 */
	public Date getLastATMUsed() {
		return lastATMUsed;
	}

	/**
	 * @param lastATMUsed the {@linkplain #lastATMUsed} to set
	 */
	public void setLastATMUsed(Date lastATMUsed) {
		this.lastATMUsed = lastATMUsed;
	}

	/**
	 * @return the {@linkplain #lastPOSUsed}
	 */
	public Date getLastPOSUsed() {
		return lastPOSUsed;
	}

	/**
	 * @param lastPOSUsed the {@linkplain #lastPOSUsed} to set
	 */
	public void setLastPOSUsed(Date lastPOSUsed) {
		this.lastPOSUsed = lastPOSUsed;
	}

	/**
	 * @return the {@linkplain #lastRefreshTime}
	 */
	public Date getLastRefreshTime() {
		return lastRefreshTime;
	}

	/**
	 * @param lastRefreshTime the {@linkplain #lastRefreshTime} to set
	 */
	public void setLastRefreshTime(Date lastRefreshTime) {
		this.lastRefreshTime = lastRefreshTime;
	}

	/**
	 * @return the {@linkplain #lastChangeStatusTime}
	 */
	public Date getLastChangeStatusTime() {
		return lastChangeStatusTime;
	}

	/**
	 * @param lastChangeStatusTime the {@linkplain #lastChangeStatusTime} to set
	 */
	public void setLastChangeStatusTime(Date lastChangeStatusTime) {
		this.lastChangeStatusTime = lastChangeStatusTime;
	}

	/**
	 * @return the {@linkplain #lastTranTime}
	 */
	public Date getLastTranTime() {
		return lastTranTime;
	}

	/**
	 * @param lastTranTime the {@linkplain #lastTranTime} to set
	 */
	public void setLastTranTime(Date lastTranTime) {
		this.lastTranTime = lastTranTime;
	}

	/**
	 * @return the {@linkplain #accounts}
	 */
	public List<Accounts> getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts the {@linkplain #accounts} to set
	 */
	public void setAccounts(List<Accounts> accounts) {
		this.accounts = accounts;
	}

	/**
	 * @return the {@linkplain #cardProfiles}
	 */
	public List<CardProfiles> getCardProfiles() {
		return cardProfiles;
	}

	/**
	 * @param cardProfiles the {@linkplain #cardProfiles} to set
	 */
	public void setCardProfiles(List<CardProfiles> cardProfiles) {
		this.cardProfiles = cardProfiles;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}
}