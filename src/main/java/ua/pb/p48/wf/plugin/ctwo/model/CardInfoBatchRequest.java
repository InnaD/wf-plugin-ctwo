package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;
/**
 * 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class CardInfoBatchRequest extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Список карт дл яполучения 
	 */
	@XmlElementWrapper(name="cards")
	@XmlElement(name="card")
	private List<String> cards;

	/**
	 * @return the {@linkplain #cards}
	 */
	public List<String> getCards() {
		return cards;
	}

	/**
	 * @param cards the  {@linkplain #cards} to set
	 */
	public void setCards(List<String> cards) {
		this.cards = cards;
	}
}