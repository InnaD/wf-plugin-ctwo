package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * Класс для запроса информации по счету
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class AccountInfoLightRequest extends Abstract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * номер превичнооткрытого счета
	 */
	@XmlElement
	private String can;
	
	/**
	 * 
	 */
	@XmlElement
	private String dataUri;

	/**
	 * @return the {@linkplain #can}
	 */
	public String getCan() {
		return can;
	}

	/**
	 * @param can the {@linkplain #can} to set
	 */
	public void setCan(String can) {
		this.can = can;
	}

	/**
	 * @return the {@linkplain #dataUri}
	 */
	public String getDataUri() {
		return dataUri;
	}

	/**
	 * @param dataUri the {@linkplain #dataUri} to set
	 */
	public void setDataUri(String dataUri) {
		this.dataUri = dataUri;
	}
}