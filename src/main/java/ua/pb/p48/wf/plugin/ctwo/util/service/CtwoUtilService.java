package ua.pb.p48.wf.plugin.ctwo.util.service;

import java.util.ArrayList;
import java.util.List;

import ua.pb.p48.wf.plugin.ctwo.model.CardInfo;

public class CtwoUtilService {
	
	public enum CardStatus{
		
		NOT_ACTIVE(0,"Не активна", "NOT ACTIVE"),
		OPENED(1,"Открыта", "OPENED"), 
		LOST(2,"Потеряна","LOST"),
		STOLEN(3,"Украдена","STOLEN"),
		RESTRICTED(4,"Ограничена","RESTRICTED"), 
		VIP(5,"Вип","VIP"), 
		INTERNAL(6,"Внутренняя", "INTERNAL"), 
		COMPROMISED(8,"Скомпрометирована", "COMPROMISED"), 
		CLOSED(9,"Закрыта", "CLOSED"), 
		REFERRAL(10,"До выяснения", "REFERRAL"), 
		DECLARED(12,"Заказана","DECLARED"), 
		EXPIRED(15,"Просрочена", "EXPIRED");		
		
		private Integer state;
		private String stateNameRu;
		private String stateNameEng;
		CardStatus(int state, String stateNameRu, String stateNameEng){
			this.state=state;
			this.stateNameRu=stateNameRu;
			this.stateNameEng=stateNameEng;
		}
		
		/**
		 * @return the {@linkplain #state}
		 */
		public Integer getState() {
			return state;
		}
		
		/**
		 * @return the Hexidecimal representation of {@linkplain #state}
		 */
		public String getStateHexidecimal() {
			return convertToHexadecimal(String.valueOf(state));
		}
		/**
		 * @return the {@linkplain #stateNameRu}
		 */
		public String getStateNameRu() {
			return stateNameRu;
		}
		/**
		 * @return the {@linkplain #stateNameEng}
		 */
		public String getStateNameEng() {
			return stateNameEng;
		}
		
		private  final String convertToHexadecimal(String number) {
			if (number != null && number.length() > 1) {
				if (number.equals("10")) {
					return "A";
				} else if (number.equals("11")) {
					return "B";
				} else if (number.equals("12")) {
					return "C";
				} else if (number.equals("13")) {
					return "D";
				} else if (number.equals("14")) {
					return "E";
				} else if (number.equals("15")) {
					return "F";
				} else {
					//возвращаем неизвестный статус
					return "U";
				}
			} else {
				return number;
			}
		}		
		
	}
	
	public static List<CardInfo> getCardInfoInState(List<CardInfo> cardInfoList, CardStatus ...crdStat){
		List<CardInfo> sortedList = new ArrayList<CardInfo>();
		for (CardInfo cardInfo : cardInfoList) {
			boolean checkState = false;
			for (CardStatus status : crdStat) {			
				//Если хоть один статус совпал, возвращаем инфо
				if (cardInfo.getStatus().compareTo(status.getState())==0) {	
					checkState = true;
					break;
				}
			}
			if(checkState){
				sortedList.add(cardInfo);
			}
		}
		return sortedList;
		
	}
	
	public static List<CardInfo> getCardInfoNotInState(List<CardInfo> cardInfoList, CardStatus ...crdStat){
		List<CardInfo> sortedList = new ArrayList<CardInfo>();
		for (CardInfo cardInfo : cardInfoList) {
			boolean checkState = true;
			for (CardStatus status : crdStat) {
				//если хоть один статус совпал, карту не добавляем
				if (cardInfo.getStatus().compareTo(status.getState())==0) {	
					checkState = false;		
					break;
				}
			}
			if(checkState){
				sortedList.add(cardInfo);
			}
		}
		return sortedList;
		
	}

}
