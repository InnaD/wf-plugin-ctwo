package ua.pb.p48.wf.plugin.ctwo.util.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

public abstract class ObjectMapperTest {


    public static final ObjectMapper mapper = new ObjectMapper();

    static {
	mapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
	mapper.disable(SerializationFeature.WRAP_ROOT_VALUE);
	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
		false);

	mapper.setAnnotationIntrospector(new AnnotationIntrospectorPair(
		new JaxbAnnotationIntrospector(TypeFactory.defaultInstance()),
		new JacksonAnnotationIntrospector()));

    }

}