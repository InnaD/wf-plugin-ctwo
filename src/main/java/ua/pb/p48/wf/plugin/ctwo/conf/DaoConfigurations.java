package ua.pb.p48.wf.plugin.ctwo.conf;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.ValidConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration (value="ctwoDaoConfigurations")
public class DaoConfigurations {

    private static final transient Logger LOG = LoggerFactory
	    .getLogger(DaoConfigurations.class);

    @Resource(name = "dbctwo.dataSources")
    private Map<String, PoolDataSource> dataSourceMap;

    @Value("#{systemProperties['WF.BANK']}")
    private String bank;

    @Resource(name = "bank2dsMapCtwo")
    private Map<String, String> bank2dsMap;

    /**
     * Perform connection validation
     * 
     * @return Connection
     * @throws SQLException
     */
    @Bean(name="ctwoValidateDatasource")
    Map<String, PoolDataSource> validateDatasource() throws SQLException {
	String uri = getUriByBank(getBank());
	for (Entry<String, PoolDataSource> entrySet : dataSourceMap.entrySet()) {
	    if(entrySet.getKey().equals(uri)) { //проверяем только коннект первого урла - это по банку
		PoolDataSource dataSource = entrySet.getValue();
		Connection vConnection = null;
		try {
		    if (LOG.isDebugEnabled()) {
			LOG.debug("Validate connection to: {}, {}",
				entrySet.getKey(), dataSource.getURL());
		    }
		    vConnection = dataSource.getConnection();
		    if (!((ValidConnection) vConnection).isValid()) {
			LOG.error("inValid connection to: {}, {}",
				entrySet.getKey(), dataSource.getURL());
			/*throw new RuntimeException("inValid connection:"
							+ ((PoolDataSource) dataSource).getURL());*/
		    }

		} catch (SQLException e) {
		    if (vConnection == null) {
			LOG.error("error create connection to: {}, {}",
				entrySet.getKey(), dataSource.getURL());
			/*throw new RuntimeException("error create connection to:"
							+ ((PoolDataSource) dataSource).getURL());*/
		    }

		}

		if (vConnection != null) {
		    // return connection to pool
		    vConnection.close();
		    vConnection = null;
		}
	    }
	}

	return dataSourceMap;
    }

    /**
     * @return the {@linkplain #dataSourceMap}
     */
    public Map<String, PoolDataSource> getDataSourceMap() {
	return dataSourceMap;
    }

    /**
     * @param dataSourceMap
     *            the {@linkplain #dataSourceMap} to set
     */
    public void setDataSourceMap(Map<String, PoolDataSource> dataSourceMap) {
	this.dataSourceMap = dataSourceMap;
    }

    /**
     * @return the {@linkplain #bank}
     */
    public String getBank() {
	return bank;
    }

    public String getUriByBank(String bank) {
	return bank2dsMap.get(bank);
    }

}
