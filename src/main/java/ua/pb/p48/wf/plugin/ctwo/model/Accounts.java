package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * 
 * @author Mazan
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Accounts extends Abstract implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * счет
	 */
	@XmlElement
	private String account;
	/**
	 * статус счета, связанного с картой
	 */
	@XmlElement
	private String status;
	/**
	 * описание счета связанного с картой
	 */
	@XmlElement
	private String acctdescr;
	/**
	 * общий баланс
	 */
	@XmlElement
	private BigDecimal ledgerbalance;
	/**
	 * доступный баланс
	 */
	@XmlElement
	private BigDecimal availbalance;
	/**
	 * валюта
	 */
	@XmlElement
	private Long currency;

	/**
	 * @return the {@linkplain #account}
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account
	 *            the {@linkplain #account} to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the {@linkplain #status}
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the {@linkplain #status} to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the {@linkplain #acctdescr}
	 */
	public String getAcctdescr() {
		return acctdescr;
	}

	/**
	 * @param acctdescr
	 *            the {@linkplain #acctdescr} to set
	 */
	public void setAcctdescr(String acctdescr) {
		this.acctdescr = acctdescr;
	}

	/**
	 * @return the {@linkplain #ledgerbalance}
	 */
	public BigDecimal getLedgerbalance() {
		return ledgerbalance;
	}

	/**
	 * @param ledgerbalance
	 *            the {@linkplain #ledgerbalance} to set
	 */
	public void setLedgerbalance(BigDecimal ledgerbalance) {
		this.ledgerbalance = ledgerbalance;
	}

	/**
	 * @return the {@linkplain #availbalance}
	 */
	public BigDecimal getAvailbalance() {
		return availbalance;
	}

	/**
	 * @param availbalance
	 *            the {@linkplain #availbalance} to set
	 */
	public void setAvailbalance(BigDecimal availbalance) {
		this.availbalance = availbalance;
	}

	/**
	 * @return the {@linkplain #currency}
	 */
	public Long getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the {@linkplain #currency} to set
	 */
	public void setCurrency(Long currency) {
		this.currency = currency;
	}

	/**
	 * Статусы связи счета с картой
	 * 
	 * @author Domash
	 * 
	 */
	public static interface AccountToCardState {
		/**
		 * Открытый
		 */
		String OPENED = "1";
		/**
		 * Только приход
		 */
		String ONLY_DEBET = "2";
		/**
		 * Первично открытый
		 */
		String PRIMARY_OPENED = "3";
		/**
		 * Только просмотр
		 */
		String ONLY_READ = "5";
		/**
		 * Закрытый
		 */
		String CLOSED = "9";
	}
}
