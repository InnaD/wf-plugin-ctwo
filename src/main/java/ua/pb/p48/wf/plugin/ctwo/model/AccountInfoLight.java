package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;
/**
 * 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class AccountInfoLight extends Abstract implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@XmlElement
	private String pan;
	/**
	 * 
	 */
	@XmlElement
	private String account;
	/**
	 * 
	 */
	@XmlElement
	private Long status;
	/**
	 * 
	 */
	@XmlElement
	private String acctdescr;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal ledgerbalance;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal availbalance;
	/**
	 * 
	 */
	@XmlElement
	private Long currency;
	
	
	/**
	 * 
	 */
	@XmlElement
	private Integer fiid;
	
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal debithold;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal credithold;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal bonus;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal overdraftlimit;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal tmpoverdraft;
	/**
	 * 
	 */
	@XmlElement
	private Date tmpoverdraftexpiration;
	/**
	 * 
	 */
	@XmlElement
	private BigDecimal remain;
	
	/**
	 * @return the {@linkplain #pan}
	 */
	public String getPan() {
		return pan;
	}
	/**
	 * @param pan the {@linkplain #pan} to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}
	/**
	 * @return the {@linkplain #account}
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the {@linkplain #account} to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the {@linkplain #status}
	 */
	public Long getStatus() {
		return status;
	}
	/**
	 * @param status the {@linkplain #status} to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}
	/**
	 * @return the {@linkplain #acctdescr}
	 */
	public String getAcctdescr() {
		return acctdescr;
	}
	/**
	 * @param acctdescr the {@linkplain #acctdescr} to set
	 */
	public void setAcctdescr(String acctdescr) {
		this.acctdescr = acctdescr;
	}
	/**
	 * @return the {@linkplain #ledgerbalance}
	 */
	public BigDecimal getLedgerbalance() {
		return ledgerbalance;
	}
	/**
	 * @param ledgerbalance the {@linkplain #ledgerbalance} to set
	 */
	public void setLedgerbalance(BigDecimal ledgerbalance) {
		this.ledgerbalance = ledgerbalance;
	}
	/**
	 * @return the {@linkplain #availbalance}
	 */
	public BigDecimal getAvailbalance() {
		return availbalance;
	}
	/**
	 * @param availbalance the {@linkplain #availbalance} to set
	 */
	public void setAvailbalance(BigDecimal availbalance) {
		this.availbalance = availbalance;
	}
	/**
	 * @return the {@linkplain #currency}
	 */
	public Long getCurrency() {
		return currency;
	}
	/**
	 * @param currency the {@linkplain #currency} to set
	 */
	public void setCurrency(Long currency) {
		this.currency = currency;
	}
	/**
	 * @return the {@linkplain #fiid}
	 */
	public Integer getFiid() {
		return fiid;
	}
	/**
	 * @param fiid the {@linkplain #fiid} to set
	 */
	public void setFiid(Integer fiid) {
		this.fiid = fiid;
	}
	/**
	 * @return the {@linkplain #debithold}
	 */
	public BigDecimal getDebithold() {
		return debithold;
	}
	/**
	 * @param debithold the {@linkplain #debithold} to set
	 */
	public void setDebithold(BigDecimal debithold) {
		this.debithold = debithold;
	}
	/**
	 * @return the {@linkplain #credithold}
	 */
	public BigDecimal getCredithold() {
		return credithold;
	}
	/**
	 * @param credithold the {@linkplain #credithold} to set
	 */
	public void setCredithold(BigDecimal credithold) {
		this.credithold = credithold;
	}
	/**
	 * @return the {@linkplain #bonus}
	 */
	public BigDecimal getBonus() {
		return bonus;
	}
	/**
	 * @param bonus the {@linkplain #bonus} to set
	 */
	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}
	/**
	 * @return the {@linkplain #overdraftlimit}
	 */
	public BigDecimal getOverdraftlimit() {
		return overdraftlimit;
	}
	/**
	 * @param overdraftlimit the {@linkplain #overdraftlimit} to set
	 */
	public void setOverdraftlimit(BigDecimal overdraftlimit) {
		this.overdraftlimit = overdraftlimit;
	}
	/**
	 * @return the {@linkplain #tmpoverdraft}
	 */
	public BigDecimal getTmpoverdraft() {
		return tmpoverdraft;
	}
	/**
	 * @param tmpoverdraft the {@linkplain #tmpoverdraft} to set
	 */
	public void setTmpoverdraft(BigDecimal tmpoverdraft) {
		this.tmpoverdraft = tmpoverdraft;
	}
	/**
	 * @return the {@linkplain #tmpoverdraftexpiration}
	 */
	public Date getTmpoverdraftexpiration() {
		return tmpoverdraftexpiration;
	}
	/**
	 * @param tmpoverdraftexpiration the {@linkplain #tmpoverdraftexpiration} to set
	 */
	public void setTmpoverdraftexpiration(Date tmpoverdraftexpiration) {
		this.tmpoverdraftexpiration = tmpoverdraftexpiration;
	}
	/**
	 * @return the {@linkplain #remain}
	 */
	public BigDecimal getRemain() {
		return remain;
	}
	/**
	 * @param remain the {@linkplain #remain} to set
	 */
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}	
}