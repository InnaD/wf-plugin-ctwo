package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * Інформація по рахунку з TWO
 * @author RII
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class AcctInfo extends Abstract implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int DEFAULT_PFIID = 1;
	
	/**
	 * Статус рахунку
	 */
	@XmlElement 
	private Integer pStatus;  
	/**
	 * Валюта рахунку
	 */
	@XmlElement 
	private Integer pCurrency;  
	/**
	 * Загальний залишок
	 */
	@XmlElement 
	private BigDecimal pLedger;  
	/**
	 * Доступний залишок
	 */
	@XmlElement 
	private BigDecimal pAvail;  
	/**
	 * Сума витратних авторизацій
	 */
	@XmlElement 
	private BigDecimal pDebitHold;  
	/**
	 * Сума депозитних авторизацій
	 */
	@XmlElement 
	private BigDecimal pCreditHold; 
	/**
	 * Бонус/борг
	 * <br> Якщо > 0 - бонус
	 * <br> Якщо < 0 - борг
	 */
	@XmlElement 
	private BigDecimal pBonus; 
	/**
	 * Допустима перевитрата
	 */
	@XmlElement 
	private BigDecimal pOverdraft; 
	/**
	 * Тимчасовий овердрафт
	 */
	@XmlElement 
	private BigDecimal pTmpOverdraft; 
	/**
	 * Термін дії тимчасового овердрафту
	 */
	@XmlElement 
	private Date pTmpOverdraftExpiration; 
	/**
	 * Банківський залишок по рахунку
	 */
	@XmlElement 
	private BigDecimal pRemain; 
	/**
	 * Перелік карток, пов'язаних з рахунком
	 */
	@XmlElement(name = "card")
	@XmlElementWrapper(name = "cards")
	private List<Card> pCards;
	
	/**
	 * @return the {@linkplain #pStatus}
	 */
	public Integer getpStatus() {
		return pStatus;
	}
	/**
	 * @param pStatus the {@linkplain #pStatus} to set
	 */
	public void setpStatus(Integer pStatus) {
		this.pStatus = pStatus;
	}
	/**
	 * @return the {@linkplain #pCurrency}
	 */
	public Integer getpCurrency() {
		return pCurrency;
	}
	/**
	 * @param pCurrency the {@linkplain #pCurrency} to set
	 */
	public void setpCurrency(Integer pCurrency) {
		this.pCurrency = pCurrency;
	}
	/**
	 * @return the {@linkplain #pLedger}
	 */
	public BigDecimal getpLedger() {
		return pLedger;
	}
	/**
	 * @param pLedger the {@linkplain #pLedger} to set
	 */
	public void setpLedger(BigDecimal pLedger) {
		this.pLedger = pLedger;
	}
	/**
	 * @return the {@linkplain #pAvail}
	 */
	public BigDecimal getpAvail() {
		return pAvail;
	}
	/**
	 * @param pAvail the {@linkplain #pAvail} to set
	 */
	public void setpAvail(BigDecimal pAvail) {
		this.pAvail = pAvail;
	}
	/**
	 * @return the {@linkplain #pDebitHold}
	 */
	public BigDecimal getpDebitHold() {
		return pDebitHold;
	}
	/**
	 * @param pDebitHold the {@linkplain #pDebitHold} to set
	 */
	public void setpDebitHold(BigDecimal pDebitHold) {
		this.pDebitHold = pDebitHold;
	}
	/**
	 * @return the {@linkplain #pCreditHold}
	 */
	public BigDecimal getpCreditHold() {
		return pCreditHold;
	}
	/**
	 * @param pCreditHold the {@linkplain #pCreditHold} to set
	 */
	public void setpCreditHold(BigDecimal pCreditHold) {
		this.pCreditHold = pCreditHold;
	}
	/**
	 * @return the {@linkplain #pBonus}
	 */
	public BigDecimal getpBonus() {
		return pBonus;
	}
	/**
	 * @param pBonus the {@linkplain #pBonus} to set
	 */
	public void setpBonus(BigDecimal pBonus) {
		this.pBonus = pBonus;
	}
	/**
	 * @return the {@linkplain #pOverdraft}
	 */
	public BigDecimal getpOverdraft() {
		return pOverdraft;
	}
	/**
	 * @param pOverdraft the {@linkplain #pOverdraft} to set
	 */
	public void setpOverdraft(BigDecimal pOverdraft) {
		this.pOverdraft = pOverdraft;
	}
	/**
	 * @return the {@linkplain #pTmpOverdraft}
	 */
	public BigDecimal getpTmpOverdraft() {
		return pTmpOverdraft;
	}
	/**
	 * @param pTmpOverdraft the {@linkplain #pTmpOverdraft} to set
	 */
	public void setpTmpOverdraft(BigDecimal pTmpOverdraft) {
		this.pTmpOverdraft = pTmpOverdraft;
	}
	/**
	 * @return the {@linkplain #pTmpOverdraftExpiration}
	 */
	public Date getpTmpOverdraftExpiration() {
		return pTmpOverdraftExpiration;
	}
	/**
	 * @param pTmpOverdraftExpiration the {@linkplain #pTmpOverdraftExpiration} to set
	 */
	public void setpTmpOverdraftExpiration(Date pTmpOverdraftExpiration) {
		this.pTmpOverdraftExpiration = pTmpOverdraftExpiration;
	}
	/**
	 * @return the {@linkplain #pRemain}
	 */
	public BigDecimal getpRemain() {
		return pRemain;
	}
	/**
	 * @param pRemain the {@linkplain #pRemain} to set
	 */
	public void setpRemain(BigDecimal pRemain) {
		this.pRemain = pRemain;
	}
	/**
	 * @return the {@linkplain #pCards}
	 */
	public List<Card> getpCards() {
		return pCards;
	}
	/**
	 * @param pCards the {@linkplain #pCards} to set
	 */
	public void setpCards(List<Card> pCards) {
		this.pCards = pCards;
	} 
}
