package ua.pb.p48.wf.plugin.ctwo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.PluginResult;
/**
 * 
 * @author Mazan
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class CopyCrdLimitResult extends PluginResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@XmlElement
	private Integer result;

	/**
	 * @return the {@linkplain #result}
	 */
	public Integer getResult() {
		return result;
	}

	/**
	 * @param result the {@linkplain #result} to set
	 */
	public void setResult(Integer result) {
		this.result = result;
	}
}