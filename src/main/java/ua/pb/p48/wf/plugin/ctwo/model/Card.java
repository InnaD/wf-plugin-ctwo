package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ua.pb.p48.wf.plugin.base.model.Abstract;

/**
 * Інформація по карткам, пов'язаних з рахунком, з TWO
 * @author RII
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Card extends Abstract implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Номер картки
	 */
	@XmlElement 
	private String pan;  
	/**
	 * Номер власника картки
	 */
	@XmlElement 
	private Integer mbr;  
	/**
	 * Опис картки, пов'язаної з рахунком
	 */
	@XmlElement 
	private String carddescr;  
	/**
	 * Унікальний ідентифікатор картки
	 */
	@XmlElement 
	private String cardUID;  
	/**
	 * Статус картки, пов'язаної з рахунком
	 */
	@XmlElement 
	private String status;
	
	/**
	 * @return the {@linkplain #pan}
	 */
	public String getPan() {
		return pan;
	}
	/**
	 * @param pan the {@linkplain #pan} to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}
	/**
	 * @return the {@linkplain #mbr}
	 */
	public Integer getMbr() {
		return mbr;
	}
	/**
	 * @param mbr the {@linkplain #mbr} to set
	 */
	public void setMbr(Integer mbr) {
		this.mbr = mbr;
	}
	/**
	 * @return the {@linkplain #carddescr}
	 */
	public String getCarddescr() {
		return carddescr;
	}
	/**
	 * @param carddescr the {@linkplain #carddescr} to set
	 */
	public void setCarddescr(String carddescr) {
		this.carddescr = carddescr;
	}
	/**
	 * @return the {@linkplain #cardUID}
	 */
	public String getCardUID() {
		return cardUID;
	}
	/**
	 * @param cardUID the {@linkplain #cardUID} to set
	 */
	public void setCardUID(String cardUID) {
		this.cardUID = cardUID;
	}
	/**
	 * @return the {@linkplain #status}
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the {@linkplain #status} to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}  
}
