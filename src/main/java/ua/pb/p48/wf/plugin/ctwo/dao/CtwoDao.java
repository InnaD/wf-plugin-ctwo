package ua.pb.p48.wf.plugin.ctwo.dao;

import java.util.List;

import ua.pb.p48.wf.plugin.base.dao.BaseDao;
import ua.pb.p48.wf.plugin.ctwo.model.*;


/**
 * Дао к компасу фронту 
 * @author kpi
 *
 */
public interface CtwoDao extends BaseDao {
	
	/**
	 * Получить с фронта статус карты 
	 * @param pan
	 * @return
	 */
	public String getCrdStat(String pan);
	
	
	/**
	 * Лимиты для копирования ПЦ
	 * @param pFromPAN - номер карты-источника
	 * @param pToPAN - номер карты-приемника
	 * @return
	 */
	public Integer copyCrdLimits(String pFromPAN, String pToPAN);
	
	/**
	 * 
	 * @param pan
	 * @return
	 */
	public CardInfo getCardInfo(String pan);	
	
	/**
	 * Замена методу {@linkplain CtwoDao#getAcctInfo(String, Integer)}
	 * 
	 * @param pAccount
	 * @param dataUri
	 * @return
	 */
	public AcctInfo getAcctInfo(String pAccount, String dataUri);
	
	
	/**
	 * Получения лимита по карте
	 * @param pan номер карты
	 * @return {@linkplain LimitInfo}
	 */
	public List<LimitInfo> getCardLimit(String pan);
	
	/**
	 * Получение лимита по счету
	 * @param pAccount номер счета
	 * @return {@linkplain LimitInfo}
	 */
	public List<LimitInfo> getAccountLimit(String pAccount, String dataUri);
	
	/**
	 * Получение информации по списку карт
	 * @param cards
	 * @return {@linkplain CardInfo}
	 */
	public List<CardInfo> getCardInfoLight(List<String> cards);
	
	/**
	 * Получение информации о счете по номеру карты
	 * @param cards
	 * @return {@linkplain AccountInfoLight}
	 */
	public List<AccountInfoLight> getAccountsByCardLight(List<String> cards); 
	
	/**
	 * Получение информации по списку счетов
	 * @param accInfoRequest
	 * @return {@linkplain AccountInfoLight}
	 */
	public List<AccountInfoLight> getAccountInfoLight(
			List<AccountInfoLightRequest> accInfoRequest);
	
	/**
	 * Отримання ліміту картки
	 * 
	 * @author RII
	 * @param pan - номер картки
	 * @return {@linkplain LimitInfo}
	 */
	public List<LimitInfo> getCardAccountLimit(String pan);
	
	/**
	 * Копирование лимитов по картсчетам
	 * @author Podkorytov
	 * @param dataUri
	 * @param pFromAcc - номер счета-источника
	 * @param dataUriPanOld
	 * @param pToAcc - номер счета-приемника
	 * @return
	 */
	public Integer copyAccLimits(String dataUri, String pFromAcc, String dataUriPanOld, String pToAcc);

}