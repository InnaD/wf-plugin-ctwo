package ua.pb.p48.wf.plugin.ctwo.model;

import ua.pb.p48.wf.plugin.base.model.BasePluginException;

/**
 * Ошибки плагина
 * @author kpi
 *
 */
public class CtwoException extends BasePluginException {

	public static final String CODE_NOSFBA = "NOSFBA";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CtwoException(String prCode, String prMess) {
        super(prCode, prMess);
    }
}
