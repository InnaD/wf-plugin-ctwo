package ua.pb.p48.wf.plugin.ctwo.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.*;

import ua.pb.p48.wf.plugin.base.lang.annotation.JmxTest;
import ua.pb.p48.wf.plugin.base.lang.annotation.JmxTestCicle;
import ua.pb.p48.wf.plugin.base.service.BasePluginService;
import ua.pb.p48.wf.plugin.ctwo.model.*;


/**
 * Для работы с компасом фронт 
 * @author kpi
 *
 */
@Path("/CtwoService/")
@Produces({ "application/xml; charset=UTF-8", "application/json; charset=UTF-8" })
@Consumes({ "application/xml; charset=UTF-8", "application/json; charset=UTF-8" })
@WebService
public interface CtwoService extends BasePluginService {
	
	/**
	 * Установить текущий для потока dsUri
	 * @param dsUri
	 */
	@WebMethod
	public void setDsUri(@WebParam(name = "dsUri")@QueryParam(value = "dsUri") String dsUri);

	
	/**
	 * FIXME @Deprecated  </br>
	 * 
	 * Получить с фронта статус карты crdStat
	 * @param pan
	 * @return
	 */
	@Deprecated
	@JmxTest(testCicle=JmxTestCicle.TEMPORARILY_NOT_COVERED)
	@POST
	@Path("/getCrdStat/")
	@WebMethod
	@WebResult(name="crdStat")
	public String getCrdStat(
			@WebParam(name = "pan")@QueryParam(value = "pan") String pan);	
	
	/**
	 * Отримати інформацію по рахунку
	 * 
	 * @author RII
	 * @param pAccount - номер рахунку (can)
	 * @param dataUri 
	 * @see AcctInfo
	 * @return AcctInfo
	 */
	@WebMethod
	@POST
	@Path("/getAcctInfoWithDataUri/")
	@WebResult(name="acctInfo")
	public AcctInfo getAcctInfoWithDataUri(
			@WebParam(name = "pAccount")@QueryParam(value = "pAccount") String pAccount,
			@WebParam(name = "dataUri")@QueryParam(value = "dataUri") String dataUri);
	
	/**
	 * 
	 * @param pan
	 * @return
	 */
	@WebMethod
	@POST
	@Path("/getCardInfo/")
	@WebResult(name = "cardInfo")
	public CardInfo getCardInfo(@WebParam(name = "pan")@QueryParam(value = "pan") String pan);
	
	/**
	 * Получения лимита по карте
	 * @param pan номер карты
	 * @return
	 */
	@WebMethod
	@POST
	@Path("/getCardLimit/")
	@WebResult(name = "limitInfo")	
	public List<LimitInfo> getCardLimit(@WebParam(name = "pan")@QueryParam(value = "pan") String pan);
	
	/**
	 * Получение лимита по счету
	 * @param pAccount номер счета
	 * @return {@linkplain LimitInfo}
	 */
	@WebMethod
	@POST
	@Path("/getAccountLimit/")
	@WebResult(name = "limitInfo")
	public List<LimitInfo> getAccountLimit(
			@WebParam(name = "pAccount")@QueryParam(value = "pAccount") String pAccount,
			@WebParam(name = "dataUri")@QueryParam(value = "dataUri") String dataUri);
	
	/**
	 * Получение информации по списку карт
	 * @param cards
	 * @return {@linkplain CardInfo}
	 */
	@WebMethod
	@POST
	@Path("/getCardInfoLight/")
	@WebResult(name = "cardInfo")
	public List<CardInfo> getCardInfoLight(
			@WebParam(name = "cards")@QueryParam(value = "cards") List<String> cards);
	
	/**
	 * FIXME @Deprecated  </br>
	 * 
	 * Получение информации о счете по номеру карты
	 * @param cards
	 * @return {@linkplain AccountInfoLight}
	 */
	@Deprecated
	@JmxTest(testCicle=JmxTestCicle.TEMPORARILY_NOT_COVERED)
	@WebMethod
	@POST
	@Path("/getAccountsByCardLight/")
	@WebResult(name = "accountInfoLight")
	public List<AccountInfoLight> getAccountsByCardLight(
			@WebParam(name = "cards")@QueryParam(value = "cards") List<String> cards);
	
	/**
	 * Получение информации по списку счетов
	 * @param accInfoRequest
	 * @return {@linkplain AccountInfoLight}
	 */
	@WebMethod
	@POST
	@Path("/getAccountInfoLight/")
	@WebResult(name = "accountInfoLight")
	public List<AccountInfoLight> getAccountInfoLight(
			@WebParam(name = "accInfoRequest")@QueryParam(value = "accInfoRequest") List<AccountInfoLightRequest> accInfoRequest);
	/**
	 * Лимиты для копирования ПЦ
	 * @param pFromPAN
	 * @param pToPAN
	 * @return
	 */
	@WebMethod
	@POST
	@Path("/copyCrdLimits/")
	@WebResult(name = "copyCrdLimits")
	public CopyCrdLimitResult copyCrdLimits(
			@WebParam(name = "pFromPAN")@QueryParam(value = "pFromPAN") String pFromPAN,
			@WebParam(name = "pToPAN")@QueryParam(value = "pToPAN") String pToPAN);
	
	/**
	 * Отримання ліміту картки
	 * 
	 * @author RII
	 * @param pan - номер картки
	 * @return {@linkplain LimitInfo}
	 */
	@WebMethod
	@POST
	@Path("/getCardAccountLimit/")
	@WebResult(name = "limitInfo")	
	public List<LimitInfo> getCardAccountLimit(@WebParam(name = "pan") String pan);
	
	/**
	 * Копирование лимитов по картсчетам
	 * @author Podkorytov
	 * @param dataUri
	 * @param pFromAcc - номер счета-источника
	 * @param dataUriPanOld
	 * @param pToAcc - номер счета-приемника
	 * @return
	 */
	@WebMethod
	@POST
	@Path("/copyAccLimits/")
	@WebResult(name = "copyAccLimits")
	public CopyCrdLimitResult copyAccLimits(
			@WebParam(name = "dataUri") String dataUri,
			@WebParam(name = "pFromAcc") String pFromAcc,
			@WebParam(name = "dataUriPanOld") String dataUriPanOld,
			@WebParam(name = "pToAcc") String pToAcc);
}