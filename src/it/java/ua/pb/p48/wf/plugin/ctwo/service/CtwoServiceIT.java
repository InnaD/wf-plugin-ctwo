package ua.pb.p48.wf.plugin.ctwo.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ua.pb.p48.wf.plugin.ctwo.dao.CtwoDao;
import ua.pb.p48.wf.plugin.ctwo.model.AccountInfoLight;
import ua.pb.p48.wf.plugin.ctwo.model.AccountInfoLightRequest;
import ua.pb.p48.wf.plugin.ctwo.model.WfCtwoModelFactory;

import com.google.common.collect.Lists;

/**
 * @author inna
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:META-INF/spring/wf-plugin-datasources.xml",
	"classpath:META-INF/spring/wf-plugin-dao.xml",
"classpath:META-INF/spring/stub-wf-plugin-context.xml" })
@Transactional(propagation = Propagation.REQUIRED, value = "dbCtwoTx")
public class CtwoServiceIT  {



    @Autowired
    private CtwoService ctwoService;


    @Autowired
    @Qualifier(value="ctwoDaoImpl")
    private CtwoDao ctwoDao;

    @Test
    public void contextInitTest() {
	assertNotNull(ctwoService);
    }

    @BeforeTransaction
    public void verifyInitialDatabaseState() throws Exception {
	ctwoService.setDsUri("ua.pb.ctwo");
    }

    @AfterTransaction
    public void verifyFinalDatabaseState() {

    }


    @Test
    @Rollback(true)
    public void getCrdStat() throws IOException{
	//@Deprecated
    }

    @Test
    @Rollback(true)
    public void getAccountInfoLight() throws IOException{
	AccountInfoLightRequest accountInfoLightRequest = WfCtwoModelFactory.generateAccountInfoLightRequest();
	accountInfoLightRequest.setDataUri("ua.pb.comp");
	List<AccountInfoLightRequest> accountInfoLightRequestList = Lists.newArrayList(accountInfoLightRequest);
	List<AccountInfoLight> acctInfoLightList = ctwoService.getAccountInfoLight(accountInfoLightRequestList);
	assertTrue(acctInfoLightList.isEmpty());
    }

    @Test
    @Rollback(true)
    public void getAccountLimit() throws IOException{
	//TODO: нет процедуры
	/*List<LimitInfo> limitInfoList = ctwoService.getAccountLimit(GeneratedDataUtils.generateCan(), "ua.pb.comp");
	assertTrue(limitInfoList.isEmpty());*/

    }

    @Test
    @Rollback(true)
    public void getCardLimit() throws IOException{
	//TODO: нет процедуры
	/*List<LimitInfo> limitInfoList = ctwoService.getCardLimit(GeneratedDataUtils.generatePan());
	assertTrue(limitInfoList.isEmpty());*/
    }

    @Test
    @Rollback(true)
    public void getCardAccountLimit() throws IOException{
	//TODO: нет процедуры
	/*List<LimitInfo> limitInfoList = ctwoService.getCardAccountLimit(GeneratedDataUtils.generatePan());
	assertTrue(limitInfoList.isEmpty());*/
    }

    @Test
    @Rollback(true)
    public void copyAccLimits() throws IOException{
	//TODO: нет процедуры
    }

    @Test
    @Rollback(true)
    public void getCardInfo() throws IOException{
	//TODO: нет процедуры
    }

    @Test
    @Rollback(true)
    public void getCardInfoLight() throws IOException{
	//TODO: нет процедуры
    }

    @Test
    @Rollback(true)
    public void getAcctInfoWithDataUri() throws IOException{
	//TODO: нет процедуры
    }

    @Test
    @Rollback(true)
    public void copyCrdLimits() throws IOException{
	//TODO: нет процедуры
    }

}
