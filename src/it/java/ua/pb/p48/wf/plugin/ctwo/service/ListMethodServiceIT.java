package ua.pb.p48.wf.plugin.ctwo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.pb.p48.wf.plugin.base.util.PluginUtil;
import ua.pb.p48.wf.plugin.ctwo.model.GeneratedDataUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/wf-plugin-test.xml"})
public class ListMethodServiceIT {

    @Value("#{mavenProps['version']}")
    private String version;


    @Value("#{mavenProps['pathToFile']}")
    private String pathToFile;

    /**
     * Сохраняет в файл все методы Service классов по плагинам
     * @throws IOException
     */
    @Test
    public void getAllServiceMethod() throws IOException {
	List<String> allMethodsTest = new ArrayList<String> ();

	List<String> allMethods = new ArrayList<String> ();

	List<String> allMethodsNotTest = new ArrayList<String> ();

	//лист, содержащий методы сервиса
	allMethods.addAll(PluginUtil.getMethodsPluginService(CtwoService.class));

	//лист, содержащий методы сервиса с тестами
	allMethodsTest.addAll(PluginUtil.getMethodsPluginService(CtwoServiceIT.class));

	Set<String> methodsNotTest = Sets.difference(new HashSet<String>(allMethods), new HashSet<String>(allMethodsTest));

	allMethodsNotTest = Lists.newArrayList(methodsNotTest);

	//проверка существования директории
	PluginUtil.creatDir(pathToFile);
	//записываем в файл
	GeneratedDataUtils.writerToFileTest(pathToFile, CtwoServiceIT.class, allMethodsNotTest, version);

	assertTrue("Not all methods of test coverage. Look in the directory " + pathToFile,allMethodsNotTest.size()==0);
	assertEquals(0,allMethodsNotTest.size());

    }



    public String getApiVersion() {
	return version;
    }

}
