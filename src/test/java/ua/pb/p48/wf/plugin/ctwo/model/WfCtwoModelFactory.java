package ua.pb.p48.wf.plugin.ctwo.model;

import ua.pb.p48.wf.plugin.ctwo.util.service.ObjectMapperTest;


public class WfCtwoModelFactory extends ObjectMapperTest {


    public static AccountInfoLightRequest generateAccountInfoLightRequest() {

	AccountInfoLightRequest accountInfoLightRequest = new AccountInfoLightRequest();
	accountInfoLightRequest.setCan(GeneratedDataUtils.generateCan());

	return accountInfoLightRequest;

    }

}
