package ua.pb.p48.wf.plugin.ctwo.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.math.random.RandomData;
import org.apache.commons.math.random.RandomDataImpl;

import ua.pb.p48.wf.plugin.base.util.PluginUtil;

public class GeneratedDataUtils {

    public static final int PAN_LENGTH = 20;
    private static RandomData randomData = new RandomDataImpl();

    public static String numberToString(double d) {
	return String.format("%.2f", d).toString().replace(',', '.');
    }

    public static String generatePhone(){
	return "+38"+randomData.nextHexString(10);
    }

    /**
     * Метод генерирует ekbId в отрицательном диапазоне
     * @return
     */
    public static Long generateEkbId(){
	return randomData.nextLong(-1000000, -200000);
    }

    public static String generateString(int length){
	return randomData.nextHexString(length);
    }

    public static String generateMRef(){
	return generateString(25);
    }

    public static String generateMId(){
	return generateString(4);
    }

    public static String generatePan(){
	return randomData.nextHexString(20);
    }

    public static String generatePan(String rang){
	int randomDataLength = 20 - rang.length();
	String randomDataString = null;
	if(randomDataLength > 0){
	    randomDataString = randomData.nextHexString(randomDataLength);
	} else {
	    randomDataString = "";
	}
	return rang + randomDataString;
    }

    public static String generateSan(){
	return randomData.nextHexString(20);
    }

    public static String generateCan(){
	return randomData.nextHexString(25);
    }

    public static String generateContractType(){
	return randomData.nextHexString(4);
    }

    public static String generateProductType(){
	return randomData.nextHexString(6);
    }

    public static String generateCurrency(){
	return randomData.nextHexString(3);
    }

    public static String generateRefContract(){
	return randomData.nextHexString(25);
    }

    public static BigDecimal generateCreditLimit() {
	return BigDecimal.valueOf(randomData.nextLong(0, 1000));
    }

    public static BigDecimal generateBigDecimal() {
	return BigDecimal.valueOf(randomData.nextLong(0, 1000));
    }

    public static BigDecimal generateBigDecimal(long upLimit) {
	return BigDecimal.valueOf(randomData.nextLong(0, upLimit));
    }

    public static String generateCardName(){
	return randomData.nextHexString(150);
    }

    public static String generateCardAlias(){
	return randomData.nextHexString(150);
    }

    public static String generateExpDate(){
	return "0199";
    }

    public static String [] generatePans(int count){
	String pans [] = new String[count];
	for (int i = 0; i < count; i++) {
	    pans[i] = randomData.nextHexString(16);
	}
	return pans;
    }

    public static long generateThreadId(int upper){
	return randomData.nextInt(1, upper);
    }

    public static void writerToFileTest(String pathToFile, Class<?> aClass,
	    List<String> listDataForFile, String version) throws IOException {
	FileWriter fw = new FileWriter(pathToFile
		+ PluginUtil.getFileName(aClass.getPackage().getName()) + "_it"+"_"
		+ aClass.getSimpleName()+"_"+ version);
	BufferedWriter bw = new BufferedWriter(fw);
	for (String name : listDataForFile) {
	    bw.write(name.trim() + "\n");
	}
	bw.close();
    }
}