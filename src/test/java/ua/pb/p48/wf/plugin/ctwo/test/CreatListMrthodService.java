package ua.pb.p48.wf.plugin.ctwo.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.pb.p48.wf.plugin.base.util.PluginUtil;
import ua.pb.p48.wf.plugin.ctwo.service.CtwoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/wf-plugin-test.xml"})
public class CreatListMrthodService {
	
	@Value("#{mavenProps['version']}")
    private String version;
	
	
	@Value("#{mavenProps['pathToFile']}")
	private String pathToFile;
	
	@Value("#{mavenProps['pathToFileDepr']}")
	private String pathToFileDepr;
	/**
	 * Сохраняет в файл все методы Service классов по плагинам
	 * @throws IOException 
	 */
	@Test
    public void getAllServiceMethod() throws IOException {
		
		List<String> allMethods = new ArrayList<String> ();		
		List<String> methodsDeprecated = new ArrayList<String> ();				
			//лист, содержащий методы сервиса
		allMethods.addAll(PluginUtil.getMethodsPluginService(CtwoService.class));
			
			//проверить методы помеченые атрибутом ${JmxTestCicle.TEMPORARILY_NOT_COVERED}
		allMethods.addAll(PluginUtil.getTemporarilyNotCoveredJmxTestMethods(CtwoService.class, version));
			
			//проверить методы помеченые атрибутом ${JmxTestCicle.NOT_IMPLEMENTED_SERVICE}
		allMethods.addAll(PluginUtil.getNotImplementedServiceMethods(CtwoService.class, version));
		
		//проверить методы помеченые только атрибутом ${Deprecated}
		methodsDeprecated.addAll(PluginUtil.getDeprecatedMethods(CtwoService.class, version));
		
		
	if (!methodsDeprecated.isEmpty()) {
		PluginUtil.creatDir(pathToFileDepr);
		// записываем в файл методы помеченые Deprecated
		PluginUtil.writerToFileDeprMethods(pathToFileDepr,
				CtwoService.class, methodsDeprecated, version);
	}
			
			//проверка существования директории
			PluginUtil.creatDir(pathToFile);
	       //записываем в файл
			PluginUtil.writerToFile(pathToFile, CtwoService.class, allMethods, version);

    }
	
	
	
	public String getApiVersion() {
		return version;
	}

}
